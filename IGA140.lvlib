﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.Lib.Description" Type="Str">LabVIEW Plug and Play instrument driver for
Pyrometer IGA140 of LumaSense Technologies
 
Author: @gsi.de

Lizenziert unter EUPL V. 1.1

Copyright 2018 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstr.1, 64291 Darmstadt, Germany</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*'!!!*Q(C=\&gt;1`&lt;BJ2%-@RHS-DJ?5'&amp;KZ&gt;T27Y!D8&gt;8)(7*6?9-CV8I0!&amp;JEG2=K^!%_%7@X&gt;W&lt;"&amp;&lt;AC;7%MH\?*D^P8_@@&lt;N?K9\PUKV/VY\\CU?VT`D-[ON_/KPEP(U[K@&lt;OW.F&lt;?U5^^L8]-8\M-OPW$_-\?FX\X@I8`&lt;JY@,6`N@`@\6=/_DR?\@4*BX245GJ3A_J5K[M\*8G3*XG3*XG3"XG1"XG1"XG1/\G4/\G4/\G4'\G2'\G2'\G26S%8O=B&amp;TKYECS=,*:-G%S3&gt;I3DZ3DS**`%E(E[6?"*0YEE]C9=O3DS**`%EHM4$-#7?R*.Y%E`C9;K3:"6S0)G([26Y!E`A#4S"BS56?!*!M&amp;AQ=4!*$!7.Q9`!%XA#$T]6?!*0Y!E]A9&gt;G":\!%XA#4_"B3/V+F'9MZ(C92I\(]4A?R_.YG&amp;K/R`%Y(M@D?&amp;B/DM@R/!BH17&gt;S#()'/2W=%]@D?0ADR_.Y()`D=4QUV2XSWJF2-R:S0)&lt;(]"A?QW.YG%+'R`!9(M.D?*B7BM@Q'"\$9XB93I&lt;(]"A?!W)MSP)S*D-''JW-Q0$QK;@&amp;[CZ&amp;3;R+`WN/$[L[!61`7/I(2PUAK'_Q_M;J&lt;YB[I^5&lt;K.Y9^17L,U1.6#_MHF$&gt;51?_"_K?OK.OK2PKCLKE,KDT=?B@\HAY($1-A`&lt;\P8;\H&lt;&lt;&lt;L4;&lt;D6;LF:&lt;,J2;,B?&lt;T_&gt;P&lt;[IYS(4@H\[6H'Z[/PW.^@0CV0P\YO8Z_'*\'`*&amp;_HV7H^^+`]'\5.ZX/8P0MU1O&lt;7#!#!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="App" Type="Folder">
		<Item Name="Simple App.vi" Type="VI" URL="../Simple App.vi"/>
		<Item Name="Temperature" Type="Variable">
			<Property Name="Description:Description" Type="Str">Unit: K</Property>
			<Property Name="featurePacks" Type="Str">Description,Network</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:SingleWriter" Type="Str">True</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">False</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
		</Item>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="Default Instrument Setup.vi" Type="VI" URL="../Private/Default Instrument Setup.vi"/>
		<Item Name="Transaction.vi" Type="VI" URL="../Private/Transaction.vi"/>
	</Item>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Action-Status" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="All Parameters.vi" Type="VI" URL="../Public/Action-Status/All Parameters.vi"/>
			<Item Name="Clear Time Maximum Value Storage.vi" Type="VI" URL="../Public/Action-Status/Clear Time Maximum Value Storage.vi"/>
			<Item Name="External Clearing.vi" Type="VI" URL="../Public/Action-Status/External Clearing.vi"/>
			<Item Name="Read Ambient Temperature.vi" Type="VI" URL="../Public/Action-Status/Read Ambient Temperature.vi"/>
			<Item Name="Read Basic Temperature Range.vi" Type="VI" URL="../Public/Action-Status/Read Basic Temperature Range.vi"/>
			<Item Name="Read Clear Time Maximum Value Storage.vi" Type="VI" URL="../Public/Action-Status/Read Clear Time Maximum Value Storage.vi"/>
			<Item Name="Read Device Temperature.vi" Type="VI" URL="../Public/Action-Status/Read Device Temperature.vi"/>
			<Item Name="Read Emissivity.vi" Type="VI" URL="../Public/Action-Status/Read Emissivity.vi"/>
			<Item Name="Read Maximum Device Temperature.vi" Type="VI" URL="../Public/Action-Status/Read Maximum Device Temperature.vi"/>
			<Item Name="Read Temperature Sub Range.vi" Type="VI" URL="../Public/Action-Status/Read Temperature Sub Range.vi"/>
		</Item>
		<Item Name="Configure" Type="Folder">
			<Item Name="Ambient Temperature.vi" Type="VI" URL="../Public/Configure/Ambient Temperature.vi"/>
			<Item Name="Emissivity.vi" Type="VI" URL="../Public/Configure/Emissivity.vi"/>
			<Item Name="Temperature Sub Range.vi" Type="VI" URL="../Public/Configure/Temperature Sub Range.vi"/>
			<Item Name="Unit.vi" Type="VI" URL="../Public/Configure/Unit.vi"/>
			<Item Name="Exposure Time T90.vi" Type="VI" URL="../Public/Configure/Exposure Time T90.vi"/>
		</Item>
		<Item Name="Data" Type="Folder">
			<Item Name="Temperature.vi" Type="VI" URL="../Public/Data/Temperature.vi"/>
			<Item Name="Multiple Temperatures.vi" Type="VI" URL="../Public/Data/Multiple Temperatures.vi"/>
		</Item>
		<Item Name="Utility" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="Change Adress.vi" Type="VI" URL="../Public/Utility/Change Adress.vi"/>
			<Item Name="Change Baudrate.vi" Type="VI" URL="../Public/Utility/Change Baudrate.vi"/>
			<Item Name="Error Query.vi" Type="VI" URL="../Public/Utility/Error Query.vi"/>
			<Item Name="Interface.vi" Type="VI" URL="../Public/Utility/Interface.vi"/>
			<Item Name="Laser targeting Light.vi" Type="VI" URL="../Public/Utility/Laser targeting Light.vi"/>
			<Item Name="Reset.vi" Type="VI" URL="../Public/Utility/Reset.vi"/>
			<Item Name="Revision Query.vi" Type="VI" URL="../Public/Utility/Revision Query.vi"/>
			<Item Name="Self-Test.vi" Type="VI" URL="../Public/Utility/Self-Test.vi"/>
		</Item>
		<Item Name="Typedefs" Type="Folder">
			<Item Name="Buadrate.ctl" Type="VI" URL="../Public/Typedefs/Buadrate.ctl"/>
			<Item Name="T90 Exposure Time.ctl" Type="VI" URL="../IGA140 Readme-Dateien/T90 Exposure Time.ctl"/>
			<Item Name="Clear Max Time - Mode.ctl" Type="VI" URL="../Public/Typedefs/Clear Max Time - Mode.ctl"/>
		</Item>
		<Item Name="IMPAC-Pyrometer-IS140_IGA140_Bedienungsanleitung.pdf" Type="Document" URL="../IMPAC-Pyrometer-IS140_IGA140_Bedienungsanleitung.pdf"/>
		<Item Name="Close.vi" Type="VI" URL="../Public/Close.vi"/>
		<Item Name="Initialize.vi" Type="VI" URL="../Public/Initialize.vi"/>
		<Item Name="VI Tree.vi" Type="VI" URL="../Public/VI Tree.vi"/>
	</Item>
	<Item Name="Test" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Exampel.vi" Type="VI" URL="../Test/Exampel.vi"/>
		<Item Name="Test.vi" Type="VI" URL="../Private/Test.vi"/>
	</Item>
	<Item Name="EUPL v.1.1 - Lizenz.pdf" Type="Document" URL="../EUPL v.1.1 - Lizenz.pdf"/>
	<Item Name="EUPL v.1.1 - Lizenz.rtf" Type="Document" URL="../EUPL v.1.1 - Lizenz.rtf"/>
	<Item Name="IGA140 Readme.html" Type="Document" URL="../IGA140 Readme.html"/>
</Library>
